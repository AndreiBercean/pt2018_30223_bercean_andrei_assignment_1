package PT2018.poli.PolinomProject;

import java.util.*;

public class Polinom 
{	
	
	private class MonomComparator implements Comparator<Monom> {
	    public int compare(Monom m, Monom n) {
	        return n.getExponent() - m.getExponent();
	    }
	}
	
	private ArrayList<Monom> componente = new ArrayList<Monom>();
	
	public Polinom(){};
	
	public Polinom(String input)
	{
		String[] elements = input.split("[+]");
		String[] aux;
		for(String s : elements)
		{
			aux = s.split("[-]");
			if(aux.length<2)
			{
				toMonom(aux[0]);
			}
			else 
			{
				for(int i=0;i<aux.length;i++)
				{
					if(aux[i].length()>0)
					{
						if(i == 0)toMonom(aux[0]);
						else toMonom("-"+aux[i]);
					}
				}
			}
		}
		check();
	}
	
	public Polinom add(Polinom p)
	{
		Polinom result = p;
		ListIterator<Monom> i = componente.listIterator();
		while(i.hasNext())
		{
			result.componente.add(i.next());
		}
		result.check();
		return result;
	}
	
	public Polinom sub(Polinom p)
	{
		
		Polinom result = p.mul(new Polinom("-1"));
		ListIterator<Monom> i = componente.listIterator();
		while(i.hasNext())
		{
			result.componente.add(i.next());
		}
		result.check();
		return result;
	}
	
	public Polinom mul(Polinom p)
	{
		Polinom result = new Polinom();
		ListIterator<Monom> i = componente.listIterator();
		while(i.hasNext())
		{
			Monom a = i.next();
			ListIterator<Monom> j = p.componente.listIterator();
			while(j.hasNext())
			{
				Monom b = j.next();
				result.componente.add(a.mul(b));
			}
		}
		result.check();
		return result;
	}
	
	public Polinom div(Polinom p)
	{
		Polinom aux = this,result = new Polinom();
		while(aux.grad() >= p.grad())
		{
			Monom a = new Monom(aux.coef()/p.coef(),aux.grad()-p.grad());
			Polinom A = new Polinom();A.componente.add(a);result.componente.add(a);
			//System.out.println(result.toString());
			aux = aux.sub(A.mul(p));
			//System.out.println(aux.toString());
		}
		//System.out.println(p.toString());
		p = aux;
		
		return result;
	}
	
	public Polinom deriv()
	{
		Polinom result = new Polinom();
		ListIterator<Monom> i = componente.listIterator();
		while(i.hasNext())
		{
			Monom a = i.next();
			result.componente.add(new Monom(a.getCoef()*a.getExponent(),a.getExponent()-1));
		}
		result.check();
		return result;
	}
	
	public Polinom integ()
	{
		Polinom result = new Polinom();
		ListIterator<Monom> i = componente.listIterator();
		while(i.hasNext())
		{
			Monom a = i.next();
			result.componente.add(new Monom(a.getCoef()/(a.getExponent()+1),a.getExponent()+1));
		}
		result.check();
		return result;
	}
	
	private int grad()
	{
		check();
		ListIterator<Monom> i = componente.listIterator();
		if(i.hasNext())return i.next().getExponent();
		else return -1;
	}
	
	private double coef()
	{
		check();
		ListIterator<Monom> i = componente.listIterator();
		return i.next().getCoef();
	}
	
	private void check()
	{
		if(componente.size()>0)
		{
			sort();
			ListIterator<Monom> i = componente.listIterator();
			Monom b,a = i.next();
			if(a.getCoef() == 0)i.remove();
			int exp_a,exp_b;
			while(i.hasNext())
			{
				exp_a = a.getExponent();
				b = i.next();
				exp_b = b.getExponent();
				if(b.getCoef()==0)i.remove();
				else if(exp_a == exp_b)
				{
					Monom aux = b.add(a);
					i.set(aux);
					i.previous();
					if(i.hasPrevious()){i.previous();i.remove();}
					if(i.hasNext())i.next();
					else break;
				}
				a=b;
			}
			sort();
		}
	}
	
	public String toString()
	{
		String s = "";
		if(componente.size()>0)
		{
			check();
			ListIterator<Monom> i = componente.listIterator();
			Monom a = new Monom(0,0);
			if(i.hasNext())a = i.next();
			s = s + a.toString();
			while(i.hasNext())
			{
				a = i.next();
				s = s + a.neg() + a.toString();	
			}
		}
		return s;
	}
	
	private void toMonom(String s)
	{
		String[] aux = s.split("[*][Xx]");
		if(aux.length>1 && aux[1].charAt(0) == '^')
		{
			aux[1] = aux[1].substring(1);
			componente.add(new Monom(Integer.parseInt(aux[0]),Integer.parseInt(aux[1])));
		}
		else
		{
			componente.add(new Monom(Integer.parseInt(aux[0]),0));
		}
	}
	
	private void sort()
	{
		Collections.sort(componente, new MonomComparator());
	}
}
