package PT2018.poli.PolinomProject;

public class Monom
{
	private double coef;
	private int exp;
	
	public Monom (double c, int e)
	{
		coef = c;
		exp = e;
	}
	
	public int getExponent()
	{
		return exp;
	}
	
	public double getCoef()
	{
		return coef;
	}
	
	public String neg()
	{
		if(coef<0)return "" ;
		else return "+";
	}
	
	public Monom add(Monom x)
	{
		Monom aux;
		if(exp == x.exp)
			aux = new Monom(coef+x.coef,exp);
		else 
			aux = new Monom(0.0,-1);
		return aux;
	}
	
	public Monom sub(Monom x)
	{
		Monom aux;
		if(exp == x.exp)
			aux = new Monom(coef-x.coef,exp);
		else 
			aux = new Monom(0.0,-1);
		return aux;
	}
	
	public Monom mul(Monom x)
	{
		return new Monom(coef * x.coef,exp + x.exp);
	}
	
	public String toString()
	{
		String s = String.format("%,.2f",coef);
		if(exp <1)return s;
		else return s+"*X^"+exp;
	}
	
}
