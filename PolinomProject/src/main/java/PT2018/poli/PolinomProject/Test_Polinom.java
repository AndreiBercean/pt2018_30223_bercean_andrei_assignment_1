package PT2018.poli.PolinomProject;

import junit.framework.TestCase;

public class Test_Polinom extends TestCase {
	public void test()
	{
		Polinom x = new Polinom("1*x^2+2*x^1+1");
		Polinom y = new Polinom("2*x^4+4*X^1-3");
		String add = x.add(y).toString();
		assertEquals("2.00*X^4+1.00*X^2+6.00*X^1-2.00",add);
		String sub = x.sub(y).toString();
		assertEquals("-2.00*X^4-4.00*X^1+3.00",sub); 
		String mul = x.mul(y).toString();
		assertEquals("2.00*X^6+4.00*X^5+3.00*X^4+8.00*X^3+13.00*X^2+2.00*X^1-2.00",mul);
	}
}
