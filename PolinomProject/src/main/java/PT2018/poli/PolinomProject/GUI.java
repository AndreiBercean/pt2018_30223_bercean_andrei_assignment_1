package PT2018.poli.PolinomProject;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUI {

	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Polinom ");
		JPanel panel = new JPanel();
		Color MAROON = new Color(128, 0, 0);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600,400);
		panel.setLayout(null);
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setForeground(Color.BLACK);
		
		JLabel polinom_1_label = new JLabel("Polinomul 1");
		polinom_1_label.setBounds(10, 10, 470, 20);
		panel.add(polinom_1_label);
		
		final JTextField polinom_1_Box = new JTextField();
		polinom_1_Box.setBounds(10,30,550,35);
		polinom_1_Box.setBackground(Color.WHITE);
		polinom_1_Box.setForeground(Color.BLACK);
		panel.add(polinom_1_Box);
		
		JLabel polinom_2_label = new JLabel("Polinomul 2");
		polinom_2_label.setBounds(10, 75, 470, 20);
		panel.add(polinom_2_label);
		
		final JTextField polinom_2_Box  = new JTextField();
		polinom_2_Box.setBounds(10,95,550,35);
		polinom_2_Box.setBackground(Color.WHITE);
		polinom_2_Box.setForeground(Color.BLACK);
		panel.add(polinom_2_Box);
		
		JLabel rest_label = new JLabel("Rest(div only)");
		rest_label.setBounds(10, 300, 470, 20);
		panel.add(rest_label);
		
		final JTextField rest_Box  = new JTextField();
		rest_Box.setBounds(10,320,550,35);
		rest_Box.setBackground(Color.WHITE);
		rest_Box.setForeground(Color.BLACK);
		panel.add(rest_Box);
		
		JLabel result_label = new JLabel("Rezultat");
		result_label.setBounds(10, 240, 470, 20);
		panel.add(result_label);
		
		final JTextField result_Box = new JTextField();
		result_Box.setBounds(10,260,550,35);
		result_Box.setBackground(Color.WHITE);
		result_Box.setForeground(Color.BLACK);
		result_Box.setEditable(false);
		panel.add(result_Box);
		
		JButton add_button = new JButton("Adunare");
		add_button.setBounds(40,150,100,35);
		add_button.setBackground(MAROON);
		add_button.setForeground(Color.WHITE);
		add_button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				result_Box.setText("");
				Polinom x = new Polinom(polinom_1_Box.getText());
				Polinom y = new Polinom(polinom_2_Box.getText());
				result_Box.setText(x.add(y).toString());
			}
		});
		panel.add(add_button);
		
		JButton sub_button = new JButton("Scadere");
		sub_button.setBounds(160,150,100,35);
		sub_button.setBackground(MAROON);
		sub_button.setForeground(Color.WHITE);
		sub_button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				result_Box.setText("");
				Polinom x = new Polinom(polinom_1_Box.getText());
				Polinom y = new Polinom(polinom_2_Box.getText());
				result_Box.setText(x.sub(y).toString());
			}
		});
		panel.add(sub_button);
		
		JButton mul_button = new JButton("Inmultire");
		mul_button.setBounds(280,150,100,35);
		mul_button.setBackground(MAROON);
		mul_button.setForeground(Color.WHITE);
		mul_button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				result_Box.setText("");
				Polinom x = new Polinom(polinom_1_Box.getText());
				Polinom y = new Polinom(polinom_2_Box.getText());
				result_Box.setText(x.mul(y).toString());
			}
		});
		panel.add(mul_button);
		
		JButton div_button = new JButton("Impartire");
		div_button.setBounds(400,150,100,35);
		div_button.setBackground(MAROON);
		div_button.setForeground(Color.WHITE);
		div_button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				result_Box.setText("");
				Polinom x = new Polinom(polinom_1_Box.getText());
				Polinom y = new Polinom(polinom_2_Box.getText());
				Polinom cat =x.div(y);
				result_Box.setText(cat.toString());
				Polinom aux = x.sub(cat.mul(y));
				System.out.println(aux.toString());
				if(aux.toString().length()>0)rest_Box.setText(aux.toString());
				else rest_Box.setText("0");
			}
		});
		panel.add(div_button);
		
		JButton deriv_button = new JButton("Derivare");
		deriv_button.setBounds(100,200,100,35);
		deriv_button.setBackground(MAROON);
		deriv_button.setForeground(Color.WHITE);
		deriv_button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				result_Box.setText("");
				Polinom x = new Polinom(polinom_1_Box.getText());
				result_Box.setText(x.deriv().toString());
			}
		});
		panel.add(deriv_button);
		
		JButton integ_button = new JButton("Integrare");
		integ_button.setBounds(300,200,100,35);
		integ_button.setBackground(MAROON);
		integ_button.setForeground(Color.WHITE);
		integ_button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				result_Box.setText("");
				Polinom x = new Polinom(polinom_1_Box.getText());
				result_Box.setText(x.integ().toString());
			}
		});
		panel.add(integ_button);
		
		frame.add(panel);
		frame.setVisible(true);
	}

}